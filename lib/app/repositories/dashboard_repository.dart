import 'package:flutter/material.dart';
import '../services/index.dart';

class DashboardRepository {
  late ApiService _apiService;

  BrandRepository() {
    this._apiService = ApiService();
  }

  Future<void> mySuggestion(BuildContext context) async {
    await _apiService.mySuggestion(context);
  }
}
