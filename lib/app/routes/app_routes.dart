class Routes {
  static const root = '/';
  static const splash = '/splash_screen';
  static const intorSliderView = '/intor_slider_view';
  static const moreAboutYouView = '/more_about_you_view';
  static const bioParametersView = '/bio_parameters_view';
}
