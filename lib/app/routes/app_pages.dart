import 'package:b2b_product_app/app/module/dashboard/views/splash_view.dart';
import 'package:b2b_product_app/app/module/import_and_get_data/views/add_parameters_view.dart';
import 'package:get/get.dart' show GetPage, Transition;

import '../module/dashboard/views/bio_parameters_view.dart';
import '../module/dashboard/views/dashboard_view.dart';
import '../module/dashboard/views/intor_slider_view.dart';
import '../module/dashboard/views/more_about_you_view.dart';
import 'app_routes.dart';

class AppPages {
  static const initial = Routes.splash;

  static final routes = [
    //dashboard
    // GetPage(
    //     name: Routes.root,
    //     page: () => DashboardView(),
    //     transition: Transition.fadeIn),
    //splash
    GetPage(
        name: Routes.splash,
        page: () => SplashView(),
        transition: Transition.fadeIn),
    // intor slider view
    GetPage(
        name: Routes.intorSliderView,
        page: () => IntorSliderView(),
        transition: Transition.fadeIn),
    // more about you view
    GetPage(
        name: Routes.moreAboutYouView,
        page: () => MoreAboutYouView(),
        transition: Transition.fadeIn),
    GetPage(
        name: Routes.bioParametersView,
        page: () => BioParametersView(),
        transition: Transition.fadeIn),
  ];
}
