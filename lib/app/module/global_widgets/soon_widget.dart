part of global_widgets;

class SoonView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height - 200,
      width: Get.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            // Icons.code,
            LineIcons.laptopCode,

            color: Colors.grey.withOpacity(0.1),
            size: 200,
          ),
          Container(
            padding: EdgeInsets.only(left: 15, right: 15),
            child: Text(
              "This page under development".tr,
              textScaleFactor: 1,
              textAlign: TextAlign.center,
              style: Get.textTheme.bodyText1!.merge(TextStyle(
                fontSize: 23,
                color: Colors.grey.withOpacity(0.5),
              )),
            ),
          ),
        ],
      ),
    );
  }
}
