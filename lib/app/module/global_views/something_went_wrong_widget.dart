import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import '../../../common/index.dart';
import '../global_widgets/index.dart';

class SomethingWentWrongView extends StatelessWidget with ResponseMS {
  SomethingWentWrongView(
      {this.onTapTryAgain,
      this.width = 0,
      this.height = 0,
      this.scale = 1,
      this.response,
      this.message = ''});
  final Function? onTapTryAgain;
  final double? width;
  final double? height;
  final double? scale;
  final dynamic response;
  final String message;
  @override
  Widget build(BuildContext context) {
    return FatherWidget(
      enableGetBack: true,
      child: Scaffold(
        appBar: AppBar(
          leading: CustomBackButtonWidget(),
          elevation: 0,
          backgroundColor: Colors.transparent,
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Get.theme.scaffoldBackgroundColor,
            statusBarBrightness: Brightness.dark,
            statusBarIconBrightness: Brightness.dark,
            systemNavigationBarColor: Colors.white,
            systemNavigationBarDividerColor: Colors.white,
            systemNavigationBarIconBrightness: Brightness.light,
          ),
        ),
        body: Transform.scale(
          alignment: Alignment.center,
          scale: scale!,
          child: Container(
            width: width == 0 ? Get.width : width,
            height: height == 0 ? Get.height * .65 : height,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 25),
                Icon(
                  getErrorIcon(),
                  size: Get.width * .25,
                  color: Colors.grey.withOpacity(0.3),
                ),
                SizedBox(height: 25),
                Text(
                  getErrorTitle(),
                  style:
                      Get.textTheme.headline5!.merge(TextStyle(fontSize: 22)),
                ),
                SizedBox(height: 15),
                Text(
                  getErrorMessage(response),
                  style:
                      Get.textTheme.headline1!.merge(TextStyle(fontSize: 18)),
                ),
                SizedBox(height: 25),
                ShowHideWidget(
                  isItShow: onTapTryAgain != null,
                  child: CustomButtonWidget(
                    isItScaleTap: true,
                    isThereRadius: true,
                    width: Get.width * 0.85,
                    onTap: () {
                      Get.back();
                      Future.delayed(const Duration(milliseconds: 800), () {
                        onTapTryAgain!();
                      });
                    },
                    title: "Try Again".tr,
                  ),
                ),
                ShowHideWidget(
                  isItShow: onTapTryAgain == null,
                  child: CustomButtonWidget(
                    isItScaleTap: true,
                    isThereRadius: true,
                    width: Get.width * 0.85,
                    onTap: () {
                      Get.back();
                    },
                    title: "Back".tr,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  IconData getErrorIcon() {
    if (isNoInternetCode(response)) {
      return LineIcons.cryingFaceAlt;
    } else if (isWarningCode(response)) {
      return LineIcons.frowningFaceWithOpenMouth;
    } else {
      return LineIcons.grinningFaceWithSweat;
    }
  }

  String getErrorTitle() {
    if (response['statusCode'] == '0') {
      return "".tr;
    } else if (isWarningCode(response)) {
      return "Tray Again".tr + "!";
    } else {
      return "Something went wrong".tr;
    }
  }

  String getErrorMessage(dynamic response) {
    if (response['responseStatus'] == '0') {
      return "check your internet connection and try again".tr.tr;
    } else if (isWarningCode(response)) {
      return message.tr;
    } else {
      return "the application has encountered an unknown error".tr +
          "\n" +
          "check your internet connection and try again".tr +
          "\n" +
          'Error Code'.tr +
          ':' +
          ' ' +
          response['responseStatus'].toString();
    }
  }
}
