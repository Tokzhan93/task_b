import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';

import '../../global_widgets/index.dart';

class AddParametersView extends StatefulWidget {
  const AddParametersView({super.key});

  @override
  State<AddParametersView> createState() => _AddParametersViewState();
}

class _AddParametersViewState extends State<AddParametersView> {
  List<Widget> _questionsList = [
    Container(
      margin: EdgeInsets.only(top: 15, bottom: 10),
      padding: EdgeInsets.all(10),
      constraints: BoxConstraints(maxWidth: Get.width * .8),
      decoration: BoxDecoration(
          color: Colors.blue.withOpacity(0.1),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Text(
        'We got some parameters form your apps and devices',
        style: TextStyle(height: 1.4),
      ),
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return FatherWidget(
        child: Scaffold(
            appBar: AppBar(
                leading: IconButton(
                  icon: Icon(
                    LineIcons.angleLeft,
                    color: Colors.black,
                  ),
                  onPressed: () {},
                ),
                elevation: 0,
                actions: [
                  Center(
                      child:
                          Text('Skip', style: TextStyle(color: Colors.black))),
                  IconButton(
                    icon: Icon(
                      LineIcons.angleRight,
                      color: Colors.black,
                    ),
                    onPressed: () {},
                  ),
                ],
                centerTitle: true,
                title: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    //image
                    Center(
                        child: Text('20',
                            style: TextStyle(color: Get.theme.primaryColor))
                            ),
                  ],
                )),
            body: Container(
              margin: EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Add parameters',
                    style: TextStyle(fontSize: 18),
                  ),
                  SizedBox(height: 15),
                  Text(
                    'you can choose to not inform some of ther following by clickin on icon',
                    style: TextStyle(
                      fontSize: 17,
                      color: Colors.black54,
                      fontWeight: FontWeight.w100,
                    ),
                  ),
                  _questionsList[0],
                ],
              ),
            )));
  }
}
