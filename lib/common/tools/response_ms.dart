part of common;

mixin ResponseMS {
  bool isSuccessCode(response) {
    if (response['responseStatus'] >= 200 && response['responseStatus'] < 300) {
      return true;
    } else {
      return false;
    }
  }

  bool isNoInternetCode(response) {
    if (response['responseStatus'] == 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isWarningCode(response) {
    if (response['responseStatus'] >= 300 && response['responseStatus'] < 500) {
      return true;
    } else {
      return false;
    }
  }

  bool isErrorCode(response) {
    if (response['responseStatus'] >= 500) {
      return true;
    } else {
      return false;
    }
  }

  String handleSuccessResponse(response) {
    String successMessage = "";
    if (response.values.elementAt(1).values.length >= 1 &&
        response.values.elementAt(1).values != null) {
      response.values.elementAt(1).forEach((key, value) {
        if (key.toString() == 'message' || key.toString() == 'val') {
          successMessage = value.toString();
        }
      });
    }
    return successMessage.tr;
  }

  String handleErrorResponse(response) {
    String errorMessage = 'An unexpected error occurred'.tr;
    if (response.values.elementAt(1) is String) {
      errorMessage = response.values.elementAt(1).toString().tr + "\n";
    } else {
      if (response.values.elementAt(1).values != null) {
        response.values.elementAt(1).forEach((key, value) {
          if (key.toString() == 'message' || key.toString() == 'val') {
            errorMessage = value.toString().tr;
            errorMessage = errorMessage.tr + "\n";
          }
        });
      }
    }

    if (isWarningCode(response)) {
      return errorMessage;
    } else {
      return errorMessage +
          'Error code'.tr +
          ':' +
          ' ' +
          response.values.elementAt(0).toString();
    }
  }

  void handleOtherResponse(
      {dynamic response,
      required BuildContext context,
      Function? onTapTryAgain,
      bool messageOnly = false,
      EdgeInsets margin = const EdgeInsets.all(5),
      MessageType messageType = MessageType.flutterSnackBar}) {
    HelperMS.checkLoadingDialog();

    if (response['responseStatus'] == 0) {
      return;
    }

    if (messageOnly) {
      if (isWarningCode(response)) {
        switch (messageType) {
          case MessageType.flutterSnackBar:
            UiMS.flutterSnackBar(
                margin: margin,
                hideTitle: true,
                messageStyle: MessageStyle.warning,
                message: handleErrorResponse(response));
            break;
          case MessageType.getxSnackBar:
            Get.showSnackbar(UiMS.floatSnackBar(
                margin: margin,
                hideTitle: true,
                messageStyle: MessageStyle.warning,
                message: handleErrorResponse(response)));
            break;
          case MessageType.flutterToast:
            UiMS.flutterToast(
                messageStyle: MessageStyle.warning,
                message: handleErrorResponse(response));
            break;
        }
      } else {
        switch (messageType) {
          case MessageType.flutterSnackBar:
            UiMS.flutterSnackBar(
                margin: margin,
                hideTitle: false,
                snackBarBehavior: null,
                messageStyle: MessageStyle.error,
                message: handleErrorResponse(response));
            break;
          case MessageType.getxSnackBar:
            Get.showSnackbar(UiMS.floatSnackBar(
                margin: margin,
                hideTitle: false,
                messageStyle: MessageStyle.error,
                message: handleErrorResponse(response)));
            break;
          case MessageType.flutterToast:
            UiMS.flutterToast(
                messageStyle: MessageStyle.error,
                message: handleErrorResponse(response));
            break;
        }
      }
    } else {
      Get.to(
        () => SomethingWentWrongView(
          message: handleErrorResponse(response),
          onTapTryAgain: () {
            onTapTryAgain!();
          },
          response: response,
        ),
        transition: Transition.downToUp,
      );
      if (isWarningCode(response)) {
        switch (messageType) {
          case MessageType.flutterSnackBar:
            UiMS.flutterSnackBar(
                margin: margin,
                hideTitle: true,
                messageStyle: MessageStyle.warning,
                message: handleErrorResponse(response));
            break;
          case MessageType.getxSnackBar:
            Get.showSnackbar(UiMS.floatSnackBar(
                margin: margin,
                hideTitle: true,
                messageStyle: MessageStyle.warning,
                message: handleErrorResponse(response)));
            break;
          case MessageType.flutterToast:
            UiMS.flutterToast(
                messageStyle: MessageStyle.warning,
                message: handleErrorResponse(response));
            break;
        }
      } else {
        switch (messageType) {
          case MessageType.flutterSnackBar:
            UiMS.flutterSnackBar(
                margin: margin,
                hideTitle: false,
                snackBarBehavior: null,
                messageStyle: MessageStyle.error,
                message: handleErrorResponse(response));
            break;
          case MessageType.getxSnackBar:
            Get.showSnackbar(UiMS.floatSnackBar(
                margin: margin,
                hideTitle: false,
                messageStyle: MessageStyle.error,
                message: handleErrorResponse(response)));
            break;
          case MessageType.flutterToast:
            UiMS.flutterToast(
                messageStyle: MessageStyle.error,
                message: handleErrorResponse(response));
            break;
        }
      }
    }
  }
}

enum MessageType {
  flutterToast,
  flutterSnackBar,
  getxSnackBar,
}
